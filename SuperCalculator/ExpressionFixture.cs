﻿using System;
using System.IO;
using NUnit.Framework;

namespace SuperCalculator
{
    [TestFixture]
    public class ExpressionFixture
    {
        [TestCase("12.43")]
        public void TestPassValidExpressions(string expstr)
        {
            new Expression(expstr);
        }

        [TestCase("121345632.2132132654")]
        [TestCase("10000000")]
        [TestCase("1000000.00")]
        [TestCase("1000000.11111111111")]
        public void TestOverflownExpression(string expstr)
        {
            Assert.Throws<OverflowException>(delegate { var exp = new Expression(expstr); });
        }

        [TestCase("a")]
        [TestCase(" ")]
        [TestCase("a")]
        [TestCase("a")]
        [TestCase("a")]
        [TestCase("-")]
        [TestCase("12.4a3")]
        [TestCase("12 12")]
        [TestCase("1.1111")]
        public void TestFailExpression(string expstr)
        {
            Assert.Throws<InvalidDataException>(delegate {new Expression(expstr); });
        }

        [TestCase("2+3")]
        public  void TestValidaOperadorSuma(string expresion)
        {
            var exp = new Expression(expresion);
            Assert.True(exp.esSuma(expresion));
        }

        [TestCase("2+3", 5)]
        [TestCase("10+3", 13)]
        [TestCase("10+3+2", 15)]
        public  void TestValidaResultadoSuma(string expresion, double resultado)
        {
            var exp = new Expression(expresion);

            Assert.AreEqual(resultado, exp.Resultado);
        }

        [TestCase("5-2", 3)]
        [TestCase("10-2", 8)]
        [TestCase("10-2-5", 3)]
        public void TestValidaResultadoResta(string expression, double resultado)
        {
            var exp = new Expression(expression);

            Assert.AreEqual(resultado, exp.Resultado);
        }

        [TestCase("5+2-2", -2)]
        [TestCase("5+2+2", 2)]
        public void TestClasificaOperadores(string expression, double resultado)
        {
            var exp = new Expression(expression);
            var valoresClasificados = exp.ClasificaOperadores(expression);
            Assert.AreEqual(resultado, valoresClasificados[0]);
        }

        [TestCase("5+2-2", 5)]
        public void TestValidaOperadoresSumaResta(string expression, double resultado)
        {
            var exp = new Expression(expression);

            Assert.AreEqual(resultado,exp.Resultado);
        }
    }
}