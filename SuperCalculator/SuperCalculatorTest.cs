﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace SuperCalculator
{

    [TestFixture]
    public class SuperCalculatorTest
    {

        private Calculator _cal;

        [SetUp]
        public void SetUp()
        {
            _cal = new Calculator(-10000000, 10000000);
        }

        [TestCase("2+2")]
        [TestCase("0+4")]
        [TestCase("3+1")]
        public void SumarDosDigitosDevuelveCuatro(string a)
        {
            double result = _cal.Suma(a);
            Assert.AreEqual(4, result);
        }

        [Test]
        public void SumarDiezNumeros()
        {
            string suma = "1+2+3";
            var resultado = _cal.Suma(suma);
            Assert.AreEqual(6, resultado);
        }

        [TestCase("2,2")]
        [TestCase("2-2")]
        [TestCase("2*2")]
        [TestCase("2/2")]
        public void SumaSinSeparadorDebeRegresarInvalidDataException(string a)
        {
            Assert.Throws<InvalidDataException>(() => _cal.Suma(a));
        }

        [TestCase("10000000+10000000")]
        public void SumaValidaLongitudEnParametros(string a)
        {
            Assert.Throws<OverflowException>(() => _cal.Suma(a));
        }

        [TestCase("9999999+1")]
        public void SumaValidaResultadoLongitud(string a)
        {
            Assert.Throws<OverflowException>(() => _cal.Suma(a));
        }

        //[TestCase("1.1111+1.2212")]
        //public void SumaValidaDosDecimales(string a)
        //{
        //    Assert.Throws<InvalidDataException>(() => _cal.Suma(a));
        //}

        [TestCase("4-2")]
        public void Substract(string a)
        {
            double result = _cal.Substract(a);
            Assert.AreEqual(2, result);
        }

        [TestCase("10-5-2")]
        public void RestarVariosNumeros(string a)
        {
            double result = _cal.Substract(a);
            Assert.AreEqual(3, result);
        }

        [TestCase("123.111-125.232")]
        public void RestarValidaDosDecimales(string a)
        {
            Assert.Throws<InvalidDataException>(() => _cal.Substract(a));
        }

        [TestCase("2", "2")]
        public void MultiplicarDosPorDosRegresaCuatro(string a, string b)
        {
            double result = _cal.Multiplicar(a, b);
            Assert.AreEqual(4, result);

        }

        [TestCase("10000000", "5000")]
        public void MultiplicaValidaParametros(string a, string b)
        {
            Assert.Throws<OverflowException>(() => _cal.Multiplicar(a, b));
        }

        [TestCase("5000000", "200")]
        public void MultiplicaValidaResultado(string a, string b)
        {
            Assert.Throws<OverflowException>(() => _cal.Multiplicar(a, b));
        }

        [TestCase("112.222", "188.244")]
        public void MultiplicaValidaDosDecimales(string a, string b)
        {
            Assert.Throws<InvalidDataException>(() => _cal.Multiplicar(a, b));
        }

        [TestCase("10", "2")]
        public void Divide(string a, string b)
        {
            double result = _cal.Divide(a, b);

            Assert.AreEqual(5, result);
        }

        [TestCase("1000000", "10000001")]
        public void DivideValidaResultado(string a, string b)
        {
            Assert.Throws<OverflowException>(() => _cal.Divide(a, b));
        }

        [TestCase("25", "25.55")]
        public void DivideWithDecimals(string a, string b)
        {

            double result = _cal.Divide(a, b);

            Assert.AreEqual(0.9785, result);

        }

        [TestCase("1", "0")]
        public void DivideByZeroReturnMessage(string a, string b)
        {
            string message = "No se puede dividir entre cero";

            try
            {
                double result = _cal.Divide(a, b);
            }
            catch (Exception ex)
            {
                Assert.AreEqual(message, ex.Message);
            }
        }

        [TestCase("0", "5")]
        public void DivideZeroByFiveReturnZero(string a, string b)
        {
            double result = _cal.Divide(a, b);

            Assert.AreEqual(0, result);
        }

        [Test]
        public void SetAndGetUpperLimit()
        {
            Calculator cal = new Calculator(-5, 5);
            Assert.AreEqual(5, cal.upperLimit);
        }

        [Test]
        public void SetAndGetLowerLimit()
        {
            Calculator cal = new Calculator(-10, 10);
            Assert.AreEqual(10, cal.upperLimit);
        }

        [TestCase("11111111111113+3")]
        public void ExceedLimits(string a)
        {
            Calculator cal = new Calculator(-10000, 1000);
            try
            {
                double result = cal.Suma(a);
                Assert.Fail("This should fail: arguments exceed limits");
            }
            catch (OverflowException)
            {
                // OK
            }

        }

        [TestCase("111111113-3")]
        public void SubstractExceddingLowerLimit(string a)
        {
            try
            {
                Calculator cal = new Calculator(10, 200);

                double result = cal.Substract(a);
                Assert.Fail("This should fail: arguments exceed limits");
            }
            catch (OverflowException)
            {
                // OK
            }
        }

        [TestCase("5", "0")]
        public void test(string a, string b)
        {
            Calculator calcula = new Calculator(-100, 100);
            InvalidDataException exception = Assert.Throws<InvalidDataException>(() => calcula.Divide(a, b));
            Assert.That(exception.Message, Is.EqualTo("No se puede dividir entre cero"));
        }

        [TestCase(100, -100, 101)]
        [TestCase(100, -100, -101)]
        public void TestLimitExceeded(double upperLimit, double lowerLimit, double value)
        {
            var validator = new Validador(upperLimit, lowerLimit);
            Assert.Throws<OverflowException>(delegate { validator.ValidateLimits(value); });
        }
    }
}
