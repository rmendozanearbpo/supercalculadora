﻿using System;
using System.IO;

namespace SuperCalculator
{
    public class Validador
    {
        private double upperLimit;
        private double lowerLimit;

        public Validador(double upperLimit, double lowerLimit)
        {
            this.upperLimit = upperLimit;
            this.lowerLimit = lowerLimit;
        }

        public void ValidaDatosDeEntrada(string a)
        {
            try
            {
                double.Parse(a);
            }
            catch (Exception)
            {
                throw new InvalidDataException();
            }
        }

        public void ValidaDivisorDiferenteCero(double B)
        {
            if(B == 0)
            {
                throw new InvalidDataException("No se puede dividir entre cero");
            }
        }

        public void ValidaSeparador(string a, char separador)
        {
            if (a.IndexOf(separador) < 0 )
            {
                throw new InvalidDataException();
            }
        }

        public void ValidaLongitud(string a)
        {
            if (a.Length > 7)
            {
                throw new OverflowException("Sobrepaso el numero de caracteres");
            }
            
        }

        public void ValidaDosDecimales(string s)
        {
            var parametro = s.Split('.');

            if(parametro.Length > 1)
            {
                if(parametro[1].Length > 2)
                {
                    throw new InvalidDataException();
                }
            }
        }

        public void ValidateLimits(double value)
        {
            ValidateLowerLimit(value);
            ValidateUpperLimit(value);
        }

        private void ValidateUpperLimit(double value)
        {
            if (value > upperLimit)
            {
                throw new OverflowException("Upper limit exceeded");
            }
        }

        private void ValidateLowerLimit(double value)
        {
            if (value < lowerLimit)
            {
                throw new OverflowException("Lower limit exceeded");
            }
        }
    }
}
