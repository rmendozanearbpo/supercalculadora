using System;

namespace SuperCalculator
{
    public class Calculator
    {
        private char sumdecorator  = '+';

        public double lowerLimit;
        public double upperLimit;
        private Validador validator;

        public Calculator(Expression exp)
        {
            
        }

        public Calculator(double lowerLimit, double upperLimit)
        {
            this.upperLimit = upperLimit;
            this.lowerLimit = lowerLimit;
            this.validator = new Validador(upperLimit, lowerLimit);
        }

        public double Suma(string suma)
        {
            double result = 0;

            foreach (var s in suma.Split(sumdecorator))
            {
                result += double.Parse(s);
            }

            validator.ValidateLimits(result);
            validator.ValidaLongitud(result.ToString());
            return result;
        }

        public double Substract(string a)
        {
            double result = 0;
            
            validator.ValidaSeparador(a,'-');
            var elementos = a.Split('-');
            for (int i = 0; i < elementos.Length; i++)
            {
                if (i == 0)
                {
                    result = double.Parse(elementos[i]);
                    continue;
                }

                result -= double.Parse(elementos[i]);
            }
            
            validator.ValidateLimits(result);
            validator.ValidaLongitud(result.ToString());
            return result;
        }

        public double Divide(string a, string b)
        {
            validator.ValidaDatosDeEntrada(a);
            validator.ValidaDatosDeEntrada(b);
            validator.ValidaLongitud(a);
            validator.ValidaLongitud(b);
            validator.ValidaDivisorDiferenteCero(double.Parse(b));

            double result = Math.Round(double.Parse(a)/double.Parse(b), 4);
            validator.ValidateLimits(result);
            return result;
        }

        public double Multiplicar(string a, string b)
        {
            validator.ValidaDatosDeEntrada(a);
            validator.ValidaDatosDeEntrada(b);
            validator.ValidaLongitud(a);
            validator.ValidaLongitud(b);
            var result =  double.Parse(a) * double.Parse(b);
            validator.ValidaDosDecimales(a);
            validator.ValidaDosDecimales(b);
            validator.ValidaLongitud(result.ToString());
            validator.ValidateLimits(result);
            return result;
        }
    }
}