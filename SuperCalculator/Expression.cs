﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCalculator
{
    public class Expression
    {
        Validador _validador = new Validador(100000000, -100000000);

        private List<char> operadores= new List<char>(){'+','-'};

        public double Resultado{
            get
            {
                return Calcular();
            }
            set
            {
                
            } 
        }
        public Expression(string exp)
        {
            var valores = ObtenerValores(exp);
            Valida(valores);
            expression = exp;
        }

        private void Valida(List<string> valores)
        {
            foreach (var valor in valores)
            {
                _validador.ValidaDatosDeEntrada(valor);
                _validador.ValidaLongitud(valor);
                _validador.ValidaDosDecimales(valor);
            }
        }
        // Realizar funcion recursiva para separar valores de acuerdo a operador
        private List<string> ObtenerValores(string exp)
        {
            List<string> valores = new List<string>();

            valores = exp.Split(operadores.ToArray()).ToList();
            return valores;
        }


        private string expression = string.Empty;
        public double lowerLimit;
        public double UpperLimit;

        public bool esSuma(string exp)
        {
            return exp.Contains(operadores[0]);
        }

        public bool esResta(string exp)
        {
            return exp.Contains(operadores[1]);
        }

        public double Calcular()
        {
            Calculator cal = new Calculator(-10000,1000);
            double resultado = 0;

            if(esSuma(expression))
            {
                resultado = cal.Suma(expression);    
            }

            if(esResta(expression))
            {
                resultado = cal.Substract(expression);
            }


            
            return resultado;
        }


        public List<double> ClasificaOperadores(string s)
        {
            List<double> listValores = new List<double>();
            double contenedor;
            string expre;
            int i = 0;
            foreach (var VARIABLE in s)
            {
                i++;
                if (!double.TryParse(s[s.Length - i].ToString(), out contenedor))
                {
                    expre = s.Substring(s.Length - i, i);
                    listValores.Add(double.Parse(expre));
                    s = s.Substring(0, s.Length - i);
                    i = 0;
                }
                if (s.Length - 1 == 0)
                {
                    listValores.Add(double.Parse(s));
                    break;
                }

            }

            return listValores;
        }
    }
}
